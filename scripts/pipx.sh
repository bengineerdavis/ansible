#!/bin/bash

echo -n diceware,cookiecutter,borgbackup,borgmatic,coursera-dl,glances,trash-cli,xonsh,youtube-dl,tldr,termdown,magic-wormhole,howdoi,system-info,linode-cli | xargs -n 1 -d, pipx install --include-deps

pipx reinstall-all