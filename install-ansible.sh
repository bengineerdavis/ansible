#/bin/bash

# script that installs and configures ansible for control node
#
# Docs:
# https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html


# install python modules and additional ansible dependencies
# upgrade pip 
python3 -m pip install --upgrade pip --user

# TODO: make this an IF CONDITION and install if not present
# install and activate autocomplete python module
# python3 -m pip install argcomplete --user
# activate-global-python-argcomplete --user


# install ansible
# ubuntu https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-on-ubuntu
sudo apt update
sudo apt install software-properties-common
sudo add-apt-repository --yes --update ppa:ansible/ansible
sudo apt install ansible


# export environmental variables
chmod +x ./set-env.sh
./set-env.sh


# sets up passwordless ssh git ansible remote repo clones the remote with
# ansible yml config files, then runs them.

# variables in ./set-env.sh
mkdir -p "$PARENT_DIR"
git clone "$REPO $PARENT_DIR"


# permit execution of remote config repo for ansible-pull and run script
chmod +x ./ansible-pull.sh
./ansible-pull.sh