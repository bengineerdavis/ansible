#!/bin/bash

flatpak install /
me.kozec.syncthingtk 
# mass file sharing without cloud service
- org.zotero.Zotero # Zotero, for web references
      - org.kde.okular # Okular, pdf reader
      - io.dbeaver.DBeaverCommunity # DBeaver, for working with databases
      - de.manuel_kehl.go-for-it # A GTD list with built-in timer (for pomodoro)
      - com.getpostman.Postman # For working with APIs
      - com.discordapp.Discord # Discord
      - org.signal.Signal # Signal Desktop app
      - org.gnome.seahorse.Application # GPG keyring manager
      - org.videolan.VLC
      - com.obsproject.Studio
      - org.keepassxc.KeePassXC # For secrets/password management