
# Dev Env Setup
    1. create venv in project root & activate it from root
        command: python3 -m venv venv
    2. activate venv
        - command: source venv/bin/activate (Linux/(Mac?))
    3. from activated venv
        - command: python -m pip install pip-tools
        - command: pip-compile && pip-sync
            - notes:
                -pip-compile creates our 'requirements.txt'
                - pip-sync reads in requirements.txt cleans up any packages and dependencies we don't need and installs any news ones we do need.
    4. Link to Anisble docs to make sure we have bash auto-complete for all python-based CLIs
        - command: activate-global-python-argcomplete --user
            - Note: this will avoid errors and create a file in .bash_completion.d for our user
        - https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-argcomplete-with-pip

# TODO:
    - work on playing around with TODO proj list in the README
    - place it at the bottom under the rest of the documentation I want to put in here
    - look into language internationalization libraries for bash script?
    - move out directions and other docs in script to separate file
    - check out BATS for a bash-based bash script testing suite: https://github.com/sstephenson/bats

    - convert bash scripts into python scripts
        - pwls-ssh.sh
        - set-env.sh
        - ansible-pull.sh 
        - install-ansible.sh
        
        these scripts automate the install of ansible and its supporting libraries, including pulling in the repo we need to run ansible once installed.