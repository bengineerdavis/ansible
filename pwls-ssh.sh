#/bin/bash

# Automate and organize new ssh keypairs to use for passwordless login
# Extra convenience features added for remote git repos (GitHub/GitLab/BitBucket/etc)



# ssh passwordless login with $GIT_SERVICE
# sources:
# https://www.youtube.com/watch?v=wXb8EbdoOcs
# 
# docs used to create key exchange
# https://docs.gitlab.com/ee/ssh/

# export additional environmental variables
chmod +x ./set-env.sh
source ./set-env.sh



# TODO: turn this in an IF CONDITION to make sure xclip is installed
# install clipboard tool to capture key-pair for wayland/systemd Linux
# "sudo dnf install wl-clipboard" DOES NOT WORK ON GNOME (FEDORA 30)
# sudo dnf install xclip 

# TODO: add this to IF CONDITIONS to make sure ssh package is installed
# TODO: also confirm that ssh-agent is installed and running?
# confirm ssh install and make new key-pair
ssh -V

# TODO: add variable $ENCRYPT_OPTION and it's accompanying 'read' statement below.
# choose the desired remote git service, encryption type, and the filepath for our new ssh keypair
# source for if conditions on read input: https://stackoverflow.com/questions/5542016/bash-user-input-if
# using a default value for this input of 'gitlab'. source: https://stackoverflow.com/questions/2642585/read-a-variable-in-bash-with-a-default-value
read -p 'Choose which remote repo service you need keys for github/gitlab/etc (press "enter" to use "gitlab"): ' GIT_SERVICE
GIT_SERVICE=${name:-gitlab} 

# choose encryption algorithm
read -p 'Select method of encryption: ' ENCRYPT
ENCRYPT=${name:-rsa}

# before key is created, label key for easier reference
read -p 'Enter in comment/label for new ssh key (again): ' LABEL

# before key is created, label key for easier reference
read -p 'Enter in file name for new ssh key: ' KEY_FILE_NAME  


# build key directory and filepath(s)
# each file name includes the type of encryption selected
KEY_DIR="$HOME/.ssh/"
KEY_DIR+="$GIT_SERVICE/"
KEY_FILE_PATH="$KEY_DIR"
KEY_FILE_PATH+="$KEY_FILE_NAME"
KEY_FILE_PATH+="_$ENCRYPT"

# TODO: add variables $ENCRYPT and $ENCRYPT_OPTION to here-doc below
# TODO: create variable defaults (ex: "enter a value or press enter for 'this-standard-value' ")
# confirm new variables
cat <<INTERACTIVE_TEXT
We're making a key for the $GIT_SERVICE service.

We will be using $ENCRYPT encryption.

It will have this label: $LABEL

The key filename will have this name: $KEY_FILE_NAME

It will be stored on this path: $KEY_FILE_PATH

INTERACTIVE_TEXT

# echo We're working on this repo: $REPO # TODO: do I need this statement in INTERACTIVE here-doc?



# verbose mode to confirm mkdir made the right directory
mkdir -vp $KEY_DIR
echo ""

# TODO: use a terminal utility to paste key value into remote ansible repo host
# command for 4096 bit rsa-encrypted key
ssh-keygen -t $ENCRYPT -b 4096 -C $LABEL -f $KEY_FILE_PATH
echo ""

# confirm keypair
ls -alH $KEY_DIR
tree $KEY_DIR

# copy public key into clipboard
xclip -sel clip < "$KEY_FILE_PATH.pub"



# @FOCUS
# TODO: create interactive variables within the here-document 
# https://stackoverflow.com/questions/4937792/using-variables-inside-a-bash-heredoc 

# TODO: separate out success message as an IF CONDITION

cat <<SUCCESS 

SUCCESS! Your new secure shell key-pair has been made and the public key within that pair is now copied 
to your system's clipboard.  :)  

But you're not done yet... (read carefully!)

The other key in the pair--without the ".pub"--is a private key. Do NOT share this. 
"KEEP IT SECRET, KEEP IT SAFE!" -Gandalf

Once copied, your key will stay in your clipboard until you copy something else from any other copy 
function in your system/apps (such as your browser). This will effectively clear the key. Please remember 
this if your key does not successfully paste into your online git service user account (such as 
GitHub/GitLab/BitBucket)--you have likely copied something else to your clipboard before you had a chance 
to paste in the contents of the public key file. It is recommended to keep this terminal session open 
while copying your key, so you can easily reference "your-new-key.pub",if needed. 

Use this formula for reference to find and re-copy your new public key should it get deleted from your 
system's clipboard: (Linux) $HOME/.ssh/$GIT_SERVICE/your-key-name(.pub) 

Once you have successfully copied your key in your git remote service's user account, then use your 
system's copy function on anything else to clear out the key. 


FOLLOW THE BELOW INSTRUCTIONS (from your browser):

GitLab:
    - Steps:
        - Go to gitlab.com on your browser of choice and log in/sign up.
        - Go to user/account settings.
        - Select "SSH Keys" from the far left vertical menu.
        - Paste the key into the "Key" box.
            - You should use ctrl-v (paste)/your mouse's right-button to manually 
              paste the key value from the browser into the "Key" box.
        - Once you have this key in place, you can now use passwordless ssh to do
          you git push/pull commands for any repo under this account.
    
    - From docs: "If you manually copied your public SSH key make sure you copied 
                  the entire key starting with ssh-ed25519 (or ssh-rsa) and ending 
                  with your email address."
         
SUCCESS