#/bin/bash

# set repo variables
export GIT_SERVICE='gitlab'
export ACCOUNT='bengineerdavis'

# set local 'git clone' target
export PARENT_DIR="$HOME/bengineerdavis-resources"
